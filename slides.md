<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Monitoring versus Observability
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

----  ----


## The Three Pillars

are bull.

Monitoring and observability are not synonymous.

----

## For completeness

- Logs
- Metrics
- Distributed Traces

These things are not without worth as you will see.

----

## Monitoring

Is the practice of managing your known-unknowns and revolves around

- Checking values and defining thresholds
- Creating actionable alerts
- Having runbooks and personnel versed with your system

NNN

whitebox monitoring is for the known, hard failure modes of a system, the sort that lend themselves well toward exhibiting any deviation in a dashboardable manner, as it were.

----

## Observability

_is a measure of how well internal states of a system can be inferred from knowledge of its external outputs._

----

## Observability

Is the practice of managing you unknown-unknowns and revolves around

- instrumenting your code/system
- capturing an adequate level of detail

with the goal of answering any question about any state the system has gotten itself into.

----  ----

## So what should a system deliver

----

## Hard Requirements

1. Arbitrarily-wide structured raw events
2. Context persisted through the execution path
3. No indexes or schemas
4. High-cardinality, high-dimensionality
5. Ordered dimensions (for traceability)

---

## Soft Requirements

1. Client-side dynamic sampling
2. Close to Real-Time
3. A nice interface for combining dimensions

----  ----

## System Design Considerations

----  ----

## User Considerations

----  ----

## The last slide

- Monitoring is a verb. It is something you do.
- Observability is a noun. It is something you (hopefully) have.

_Monitoring tells you whether the system works. Observability lets you ask why it's not working._
